# Documentation

> This project has been moved to the [Mono Repository](https://gitlab.com/dlr-shepard/shepard/-/wikis/home) and is now read-only.

General documentation and examples are managed in this repository.

__Check out [the wiki](https://gitlab.com/dlr-shepard/documentation/-/wikis/home).__
